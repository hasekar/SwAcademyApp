# SwAcademyAppCiber

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


The app is divided into following sub modules, 
    - Skill Search (src/app/search-skill)
    - Sessoin Details
        - Session grid (src/app/search-grid)
        - Session info (src/app/search-info)

