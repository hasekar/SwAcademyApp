import { Injectable } from '@angular/core';
import {SessionDefinition} from './SessionDefinition';
import {TitleDefinition} from './TitleDefinition';
import {WeekDay} from '@angular/common';

@Injectable()
export class SessionServiceService {

  constructor() { }
  private _sessionInformation: SessionDefinition[] = [
    { type: 'day', title: 'Monday', text: '', cols: 1, rows: 1, color: '#D1C4E9', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'session', title: 'Session 1', text: 'Entrance Evaluation', cols: 2, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'session', title: 'Session 2', text: 'Intoduction to Base Camps abd SCA', cols: 1, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'session', title: 'Session 3', text: 'History and overview of PDO and team Roles', cols: 1, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'session', title: 'Session 4', text: 'Cadence(and walkthrough standup)', cols: 1, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'dailytheme', title: 'Daily theme', text: 'Orientation and Background', cols: 1, rows: 1, color: '#C5CAE9', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'day', title: 'Tuesday', text: '', cols: 1, rows: 1, color: '#D1C4E9', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'standup', title: '', text: 'Standup', cols: 1, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'session', title: 'Session 5', text: 'Basic Kanban Structure', cols: 1, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'session', title: 'Session 6', text: '"Definition of Done" and Acceptance Criteria', cols: 1, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'session', title: 'Session 7', text: 'Introduce Metrics: Cycle Time, Lead Time and "Flow"', cols: 1, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'session', title: 'Session 8', text: 'Pull Systems, WIP Limits, Bottleneck, Priorities', cols: 1, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'dailytheme', title: 'Daily theme', text: 'Intro to work management (kanban)', cols: 1, rows: 1, color: '#C5CAE9', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'day', title: 'wednesday', text: '', cols: 1, rows: 1, color: '#D1C4E9', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'standup', title: 'Session 9', text: 'Standup', cols: 1, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'session', title: 'Kanbam Game Play', text: 'game play', cols: 4, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'dailytheme', title: 'Daily theme', text: 'GetKanban game', cols: 1, rows: 1, color: '#C5CAE9', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'day', title: 'Thrusday', text: '', cols: 1, rows: 1, color: '#D1C4E9', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'standup', title: '', text: 'Standup', cols: 1, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'session', title: 'Session 10', text: 'Introduction to Writing Stories', cols: 1, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'session', title: 'Session 11', text: 'Converting Use Cases and Specs to User Stories', cols: 1, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'session', title: 'Session 12', text: 'Story Estimation and Techniques', cols: 1, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'session', title: 'Session 13', text: 'Spikes', cols: 1, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'dailytheme', title: 'Daily theme', text: 'Introduction to user stories', cols: 1, rows: 1, color: '#C5CAE9', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'day', title: 'Friday', text: '', cols: 1, rows: 1, color: '#D1C4E9', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'standup', title: '', text: 'Standup', cols: 1, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'session', title: 'Session 14', text: 'Introduction to Writing Stories', cols: 1, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'session', title: 'Session 15', text: 'Converting Use Cases and Specs to User Stories', cols: 1, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'session', title: 'Session 16', text: 'Story Estimation and Techniques', cols: 1, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'session', title: 'Session 17', text: 'Spikes', cols: 1, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'dailytheme', title: 'Daily theme', text: 'Introduction to user stories', cols: 1, rows: 1, color: '#C5CAE9', lessonPlan: '', sessionSummary: 'session summary one' },
    { type: 'day', title: 'Weekend', text: '', cols: 8, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: '' },

   ];

  private _tableTitle: TitleDefinition[] = [
    { text: 'Week', cols: 1, rows: 1, color: '#B39DDB'},
    { text: 'Day', cols: 1, rows: 1, color: '#B39DDB'},
    { text: '8:00 - 8:15', cols: 1, rows: 1, color: '#B39DDB'},
    { text: '8:15 - 9:00', cols: 1, rows: 1, color: '#B39DDB'},
    { text: '9:10 - 9:55', cols: 1, rows: 1, color: '#B39DDB'},
    { text: '10:05 - 10:50', cols: 1, rows: 1, color: '#B39DDB'},
    { text: '11:00 - 11:45', cols: 1, rows: 1, color: '#B39DDB'},
    { text: 'Daily Theme', cols: 1, rows: 1, color: '#B39DDB'},
  ];
  private _daysInWeek: string[] = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];

  get sessionInformation(): SessionDefinition[] {
    return this._sessionInformation;
  }

  get tableTitle(): { text: string; cols: number; rows: number; color: string }[] {
    return this._tableTitle;
  }

  get daysInWeek(): string[] {
    return this._daysInWeek;
  }
}
