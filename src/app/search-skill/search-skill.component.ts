import { Component, OnInit } from '@angular/core';
import { MatChipInputEvent } from '@angular/material';
import { ENTER, COMMA } from '@angular/cdk/keycodes';

@Component({
  selector: 'app-search-skill',
  templateUrl: './search-skill.component.html',
  styleUrls: ['./search-skill.component.css']
})
export class SearchSkillComponent implements OnInit {
  separatorKeysCodes = [ENTER, COMMA];
  searchSkill = [];

  visible: boolean = true;
  selectable: boolean = true;
  removable: boolean = true;
  addOnBlur: boolean = true;
  constructor() { }

  ngOnInit() {
  }

  add(event: MatChipInputEvent): void {
    let input = event.input;
    let value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.searchSkill.push(value.trim());
      console.log(this.searchSkill);
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(fruit: any): void {
    let index = this.searchSkill.indexOf(fruit);

    if (index >= 0) {
      this.searchSkill.splice(index, 1);
    }
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }
}
