import { Component, OnInit} from '@angular/core';
import {SessionDefinition} from '../SessionDefinition';
import {SessionServiceService} from '../session-service.service';
import {TitleDefinition} from '../TitleDefinition';
import {SessionDataServiceService} from '../session-data-service.service';


@Component({
  selector: 'app-session-grid',
  templateUrl: './session-grid.component.html',
  styleUrls: ['./session-grid.component.css']
})
export class SessionGridComponent implements OnInit {
   titleList: TitleDefinition[];
   sessionList: SessionDefinition[];
   daysInWeek: string[];

  currentSelectedSession: SessionDefinition;
  
  message: string;

  constructor(private sessionService: SessionServiceService, private sessionDataService: SessionDataServiceService) {
  }

  ngOnInit(): void {
    this.titleList = this.sessionService.tableTitle;
    this.sessionList = this.sessionService.sessionInformation;
    this.daysInWeek = this.sessionService.daysInWeek;

    this.sessionDataService
      .currentMessage
      .subscribe(sessionMessage => this.message = sessionMessage);

    this.sessionDataService.currentSelectedSession
      .subscribe(currentSession => this.currentSelectedSession = currentSession);
  }

  newSelectedSession(currentSelectedSession: SessionDefinition) {
    this.currentSelectedSession = currentSelectedSession;
    this.sessionDataService.changeSession(currentSelectedSession);
  }
}
