import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatMenuModule} from '@angular/material/menu';
import {MatListModule} from '@angular/material/list';
import {MatSelectModule} from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';



import { AppComponent } from './app.component';
import { SessionGridComponent } from './session-grid/session-grid.component';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCardModule, MatCheckboxModule, MatGridListModule} from '@angular/material';
import {MatToolbarModule} from '@angular/material/toolbar';
import {SessionServiceService} from './session-service.service';
import { SessionInfoComponent } from './session-info/session-info.component';
import {SessionDataServiceService} from './session-data-service.service';
import { SearchSkillComponent } from './search-skill/search-skill.component';


@NgModule({
  declarations: [
    AppComponent,
    SessionGridComponent,
    SessionInfoComponent,
    SearchSkillComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MatGridListModule,
    MatCardModule,
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatMenuModule,
    MatListModule,
    MatSelectModule,
    MatChipsModule,
    MatIconModule
    
  ],
  providers: [SessionServiceService, SessionDataServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
