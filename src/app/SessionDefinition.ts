export class SessionDefinition {
  type: string;
  title: string;
  text: string;
  cols: number;
  rows: number;
  color: string;
  lessonPlan: string;
  sessionSummary: string;
}
