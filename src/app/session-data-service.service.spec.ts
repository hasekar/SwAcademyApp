import { TestBed, inject } from '@angular/core/testing';

import { SessionDataServiceService } from './session-data-service.service';

describe('SessionDataServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SessionDataServiceService]
    });
  });

  it('should be created', inject([SessionDataServiceService], (service: SessionDataServiceService) => {
    expect(service).toBeTruthy();
  }));
});
