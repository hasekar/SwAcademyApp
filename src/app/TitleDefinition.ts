import { WeekDay } from '@angular/common';

export class TitleDefinition {
  text: string;
  cols: number;
  rows: number;
  color: string;
}
