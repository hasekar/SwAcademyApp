import { Component, OnInit } from '@angular/core';
import {SessionDefinition} from '../SessionDefinition';
import {SessionDataServiceService} from '../session-data-service.service';

@Component({
  selector: 'app-session-info',
  templateUrl: './session-info.component.html',
  styleUrls: ['./session-info.component.css']
})
export class SessionInfoComponent implements OnInit {

  currentSession: SessionDefinition;
  message: string;

  constructor(private sessionDataService: SessionDataServiceService) { }

  ngOnInit() {
    this.sessionDataService.currentMessage
      .subscribe(sessionData => this.message = sessionData);
    this.sessionDataService.currentSelectedSession
      .subscribe(currentSession => this.currentSession = currentSession);
  }

  newSelectedSession() {
    this.sessionDataService.changeMessage('Checking component to component interaction');
  }


}
