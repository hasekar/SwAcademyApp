import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  roles = [
    {value: 'Application Development Team', viewValue: 'App Dev Teams'},
    {value: 'COTS Support', viewValue: 'CTOS Support Teams'},
    {value: 'Data Teams', viewValue: 'Data Teams'}
  ];
}
