import { Injectable } from '@angular/core';
import { BehaviorSubject} from 'rxjs/BehaviorSubject';
import {SessionDefinition} from './SessionDefinition';

@Injectable()
export class SessionDataServiceService {

  private SelectedSessionSource = new BehaviorSubject<SessionDefinition>({ type: 'session', title: 'Session 1', text: 'Entrance Evaluation', cols: 1, rows: 1, color: 'lightblue', lessonPlan: '', sessionSummary: 'session summary one' });
  private messageSource = new BehaviorSubject<string>('default message');
  currentSelectedSession = this.SelectedSessionSource.asObservable();
  currentMessage = this.messageSource.asObservable();

  constructor() { }

  changeMessage(message: string) {
    // this.SelectedSessionSource.next(session);
    this.messageSource.next(message);
  }

  changeSession(currentSession: SessionDefinition) {
    this.SelectedSessionSource.next(currentSession);
  }

}
